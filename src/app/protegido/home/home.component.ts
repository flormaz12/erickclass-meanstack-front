import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private auths: AuthService) { }

  get Usuario() {
    return this.auths.usuario;
  }

  ngOnInit(): void {
    console.log('ojo',this.auths.usuario);
    
  }

}
